<?php
/*
 * This file is part of the TWT eCommerce platform package.
 *
 * (c) TWT Interactive GmbH <info@twt.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FNC\Bundle\AccountServiceBundle\Request\ParamConverter;

use Doctrine\ORM\EntityManager;
use FNC\Bundle\AccountServiceBundle\Controller\ServiceController;
use FNC\Bundle\AccountServiceBundle\Entity\Account;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class AccountNumberConverter implements ParamConverterInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     *
     * @return bool
     * @throws \Exception
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        if (stripos($request->getPathInfo(), '/service') !== 0) {
            return false;
        }

        $accountNumber = $request->attributes->get('accountNumber');
        $pin           = $request->get('pin');

        $repository = $this->em->getRepository('FNCAccountServiceBundle:Account');
        $account    = $repository->findOneByNumber($accountNumber);

        if (!$account instanceof Account || !$this->hasValidCredentials($account, $pin)) {
            throw new \Exception(
                'Invalid Account given',
                ServiceController::ERR_SERVICE_ACCOUNT_NOT_FOUND
            );
        }

        $request->attributes->set('account', $account);

        return true;
    }

    /**
     * Verifies that given account credentials are correct.
     *
     * @param Account $account
     * @param string  $pin
     *
     * @return bool
     * @author Christoph Ueberschaer <christoph.ueberschaer@twt.de>
     */
    protected function hasValidCredentials(Account $account, $pin)
    {
        if ($account->getPin() === null || $account->getPin() == $pin) {
            return true;
        }

        return false;
    }

    /**
     * @param ParamConverter $configuration
     * @return bool
     */
    public function supports(ParamConverter $configuration)
    {
        return $configuration->getClass() && is_a(
            $configuration->getClass(),
            'FNC\Bundle\AccountServiceBundle\Entity\Account',
            true
        );
    }
}
