<?php

namespace FNC\Bundle\AccountServiceBundle;

use FNC\Bundle\AccountServiceBundle\DependencyInjection\AggregatorCompilerPass;
use FNC\Bundle\AccountServiceBundle\DependencyInjection\ConverterCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class FNCAccountServiceBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ConverterCompilerPass());
        $container->addCompilerPass(new AggregatorCompilerPass());
    }
}
