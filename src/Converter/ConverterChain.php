<?php
/**
 * Created by PhpStorm.
 * User: ceilers
 * Date: 29.10.14
 * Time: 22:39
 */

namespace FNC\Bundle\AccountServiceBundle\Converter;

class ConverterChain
{
    /**
     * @var ConverterInterface[]
     */
    protected $converter;

    /**
     * @param ConverterInterface $converter
     *
     * @author Carsten Eilers <casten.eilers@twt.de>
     */
    public function addConverter(ConverterInterface $converter) {
        $this->converter[] = $converter;
    }

    /**
     * @param $object
     * @return mixed
     */
    public function convert($object)
    {
        foreach ($this->converter as $converter) {
            if ($converter->canHandle($object)) {
                return $converter->transform($object);
            }
        }
    }
}
