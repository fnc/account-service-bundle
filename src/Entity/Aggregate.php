<?php
/**
 * Created by PhpStorm.
 * User: ceilers
 * Date: 13.04.15
 * Time: 08:46
 */

namespace FNC\Bundle\AccountServiceBundle\Entity;

class Aggregate
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var Account
     */
    protected $account;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $value;

    /**
     * @var \DateTime
     */
    protected $timestamp;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->timestamp = new \DateTime();
    }

    /**
     * @return int
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return void
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param Account $account
     *
     * @return void
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return Account
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return string
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $key
     *
     * @return void
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return void
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return \DateTime
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTime $timestamp
     *
     * @return void
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }
}