<?php
/*
 * This file is part of the TWT eCommerce platform package.
 *
 * (c) TWT Interactive GmbH <info@twt.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FNC\Bundle\AccountServiceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Account
 */
class Account
{
    /**
     * @var int
     */
    const REFERENCE_CODE_DEFAULT = 0x01;

    /**
     * @var int
     */
    const REFERENCE_CODE_BACKEND = 0x02;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var boolean
     */
    private $disabled = false;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $pin;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var integer
     */
    private $balance;

    /**
     * @var History[]
     */
    private $history = [];

    /**
     * @var Aggregate[]
     */
    private $aggregates = [];

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param  integer $type
     * @return Account
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set disabled
     *
     * @param  boolean $disabled
     * @return Account
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Get disabled
     *
     * @return boolean
     */
    public function isDisabled()
    {
        return $this->disabled;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set pin
     *
     * @param  string $pin
     * @return Account
     */
    public function setPin($pin)
    {
        $this->pin = $pin;

        return $this;
    }

    /**
     * Get pin
     *
     * @return string
     */
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * Set currency
     *
     * @param  string $currency
     * @return Account
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set amount
     *
     * @param  integer $balance
     * @return Account
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set history
     *
     * @param  History[] $history
     * @return Account
     */
    public function setHistory($history)
    {
        $this->history = $history;

        return $this;
    }

    /**
     * Get History[]
     *
     * @return array
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * @return Aggregate[]
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function getAggregates()
    {
        return $this->aggregates;
    }

    /**
     * @param Aggregate[] $statistics
     *
     * @return void
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function setAggregates(array $aggregates)
    {
        foreach($aggregates as $aggregate) {
            /** @var Statistic $statistic */
            $aggregate->setAccount($this);
        }

        $this->aggregates = $aggregates;
    }

    /**
     * @param string $name
     *
     * @author Carsten Eilers <casten.eilers@twt.de>
     *
     * @return Aggregate
     */
    public function getAggregate($name)
    {
        foreach($this->aggregates as $aggregate) {
            if($aggregate->getName() === $name) {
                return $aggregate;
            }
        }

        return null;
    }

    /**
     * Adds a single aggregate to the account.
     *
     * @param Aggregate $aggregate
     *
     * @return Account
     * @author Christoph Ueberschaer <christoph.ueberschaer@twt.de>
     */
    public function addAggregate(Aggregate $aggregate)
    {
        $this->aggregates[] = $aggregate;

        return $this;
    }
}
