<?php

namespace FNC\Bundle\AccountServiceBundle\Entity;


/**
 * History
 */
class History
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var Account
     */
    private $account;

    /**
     * @var \DateTime
     */
    private $timestamp;

    /**
     * @var string
     */
    private $referenceCode;

    /**
     * @var string
     */
    private $referenceMessage;

    /**
     * @var integer
     */
    private $amount;

    /**
     * @var integer
     */
    private $new_balance;

    /**
     * @var string
     */
    private $transactionCode;

    /**
     * @var bool
     */
    private $discarded = 0;


    public function __construct()
    {
        $this->timestamp = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set account
     *
     * @param  Account $account
     * @return History
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set timestamp
     *
     * @param  \DateTime $timestamp
     * @return History
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set referenceCode
     *
     * @param  string $referenceCode
     * @return History
     */
    public function setReferenceCode($referenceCode)
    {
        $this->referenceCode = $referenceCode;

        return $this;
    }

    /**
     * Get referenceCode
     *
     * @return string
     */
    public function getReferenceCode()
    {
        return $this->referenceCode;
    }

    /**
     * @param string $referenceMessage
     */
    public function setReferenceMessage($referenceMessage)
    {
        $this->referenceMessage = $referenceMessage;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceMessage()
    {
        return $this->referenceMessage;
    }

    /**
     * Set amount
     *
     * @param  integer $amount
     * @return History
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $new_balance
     */
    public function setNewBalance($new_balance)
    {
        $this->new_balance = $new_balance;

        return $this;
    }

    /**
     * @return int
     */
    public function getNewBalance()
    {
        return $this->new_balance;
    }

    /**
     * @param string $transactionCode
     */
    public function setTransactionCode($transactionCode)
    {
        $this->transactionCode = $transactionCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionCode()
    {
        return $this->transactionCode;
    }

    /**
     * @return boolean
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function isDiscarded()
    {
        return $this->discarded;
    }

    /**
     * @param boolean $exlude
     *
     * @return void
     *
     * @author Carsten Eilers <carsten.eilers@twt.de>
     */
    public function setDiscarded($discarded)
    {
        $this->discarded = $discarded;
    }
}
