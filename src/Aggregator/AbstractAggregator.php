<?php
/*
 * This file is part of the TWT eCommerce platform package.
 *
 * (c) TWT Interactive GmbH <info@twt.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FNC\Bundle\AccountServiceBundle\Aggregator;

use Doctrine\ORM\EntityManager;
use FNC\Bundle\AccountServiceBundle\Entity\Account;
use FNC\Bundle\AccountServiceBundle\Entity\Aggregate;

/**
 * AbstractAggregator
 *
 * @author Carsten Eilers <carsten.eilers@twt.de>
 * @author Christoph Ueberschaer <christoph.ueberschaer@twt.de>
 */
abstract class AbstractAggregator implements AggregatorInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * Tries to load existing aggregate or returns a new instance.
     *
     * @param Account $account
     * @param string  $name
     *
     * @return Aggregate
     * @author Christoph Ueberschaer <christoph.ueberschaer@twt.de>
     */
    protected function loadAggregate(Account $account, $name)
    {
        $aggregate = $this->em->getRepository('FNCAccountServiceBundle:Aggregate')
            ->findOneBy(array('account' => $account->getId(), 'name' => $name))
        ;

        if ($aggregate === null) {
            $aggregate = new Aggregate();
            $aggregate->setName($name);
        }

        return $aggregate;
    }
}