<?php
/**
 * Created by PhpStorm.
 * User: ceilers
 * Date: 13.04.15
 * Time: 08:51
 */

namespace FNC\Bundle\AccountServiceBundle\Aggregator;

use FNC\Bundle\AccountServiceBundle\Entity\Account;
use FNC\Bundle\AccountServiceBundle\Entity\Aggregate;

interface AggregatorInterface
{
    /**
     * @param Account         $account
     * @param AggregatorChain $chain
     *
     * @return Aggregate | Aggregate[]
     *
     * @author Carsten Eilers <casten.eilers@twt.de>
     */
    public function aggregate(Account $account, array $processedAggregates);
}