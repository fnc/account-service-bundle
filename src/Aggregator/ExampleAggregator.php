<?php
/**
 * Created by PhpStorm.
 * User: ceilers
 * Date: 13.04.15
 * Time: 09:05
 */

namespace FNC\Bundle\AccountServiceBundle\Aggregator;


use FNC\Bundle\AccountServiceBundle\Entity\Account;
use FNC\Bundle\AccountServiceBundle\Entity\Aggregate;

class ExampleAggregator extends AbstractAggregator
{
    const AGGREGATE_NAME = 'example';

    /**
     * @inheritdoc
     */
    public function aggregate(Account $account, array $processedAggregates)
    {
        $aggregate = new Aggregate();

        $aggregate->setName(self::AGGREGATE_NAME);
        $aggregate->setValue(10);

        return $aggregate;
    }
}