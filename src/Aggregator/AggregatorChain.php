<?php
/*
 * This file is part of the TWT eCommerce platform package.
 *
 * (c) TWT Interactive GmbH <info@twt.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace FNC\Bundle\AccountServiceBundle\Aggregator;

use Doctrine\Common\Collections\ArrayCollection;
use FNC\Bundle\AccountServiceBundle\Entity\Account;

/**
 * Class AggregatorChain.
 *
 * @author Christoph Ueberschaer <christoph.ueberschaer@twt.de>
 */
class AggregatorChain
{
    /**
     * @var AggregatorInterface[]
     */
    protected $aggregators = [];

    /**
     * Adds an aggregator.
     *
     * @param AggregatorInterface $aggregator
     * @author Christoph Ueberschaer <christoph.ueberschaer@twt.de>
     */
    public function addAggregator(AggregatorInterface $aggregator)
    {
        $this->aggregators[] = $aggregator;
    }

    /**
     * Iterates over all registered aggregator and executes their aggregator method
     *
     * @param Account $account
     * @return array
     * @author Christoph Ueberschaer <christoph.ueberschaer@twt.de>
     */
    public function aggregate(Account $account)
    {
        $aggregates = [];

        foreach ($this->aggregators as $aggregator) {
            $return = $aggregator->aggregate($account, $aggregates);

            if (is_array($return)) {
                $aggregates = array_merge($aggregates, $return);
            } else {
                $aggregates[$return->getName()] = $return;
            }
        }

        return $aggregates;
    }
}