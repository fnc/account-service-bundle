<?php
/**
 * Created by PhpStorm.
 * User: ceilers
 * Date: 13.04.15
 * Time: 09:41
 */

namespace FNC\Bundle\AccountServiceBundle\DependencyInjection;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class AggregatorCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('fnc_account_service.aggregator_chain')) {
            return;
        }

        $definition = $container->findDefinition(
            'fnc_account_service.aggregator_chain'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'fnc_account_service.aggregator'
        );

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall(
                'addAggregator',
                array(new Reference($id))
            );
        }
    }
}